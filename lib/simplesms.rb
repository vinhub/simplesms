require "simplesms/version"

module Simplesms
  class Text
  	def initialize(to: to_number_in, message: message_to_send_in)
  		@to = to
  		@message = message
  	end

  	def send
      begin
    		url = ENV["SIMPLESMSIO_URL"]

    		body = {
    			"to" => @to,
    			"message" => @message
    		}

    		uri = URI.parse(url)

  	    http = Net::HTTP.new(uri.host, uri.port)

    		request = Net::HTTP::Post.new(uri.request_uri)
    		request.set_form_data(body)

    		response = http.request(request)
      rescue
        response = {"success": false, "message": "Error establishing http connection"}
      end
      response

  	end
  end
end
