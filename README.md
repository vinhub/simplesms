# SimpleSMS

Welcome to the SimpleSMS gem! The simpliest and cheapest way to send SMS messages from your Heroku app!

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'simplesms'
```

And then execute:

    $ bundle

Or install it yourself with:

    $ gem install simplesms

## Usage

In Heroku select a plan for the Simple SMS addon.

When you are ready to send a SMS text simply call:

```ruby
Simplesms::Text.new(to: {number_to_send_sms_including_country_code}, message: {message_to_send}).send
```

Note: remember to include the country code in the to number.

## Example

```ruby
Simplesms::Text.new(to: "15634558345", message: "Hello from SimpleSMS!").send
```

Questions? Feel free to reach out to tom@simplesms.io!


